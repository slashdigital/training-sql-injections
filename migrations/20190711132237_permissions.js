
exports.up = function(knex) {
    return knex.schema
        .createTable('permissions', function (table) {
            table.increments('id');
            table.string('key', 255).notNullable();
            table.string('value', 255).notNullable();
        });
};

exports.down = function(knex) {
    return knex.schema
        .dropTable("permissions");
};
