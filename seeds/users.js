
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        {id: 1, username: 'henry@slash.co', password: 'Rubick'},
        {id: 2, username: 'hak@slash.co', password: '13123'},
        {id: 3, username: 'vincent@slash.co', password: 'DbVinLegend'},
        {id: 4, username: 'richie@slash.co', password: 'YouCanHackMe'},
        {id: 5, username: 'theany@slash.co', password: 'AlwaysSOLO9992823'},
        {id: 6, username: 'dave@slash.co', password: 'DvE902!'},
        {id: 7, username: 'chitra@slash.co', password: '88921213D!<kL0'},
        {id: 8, username: 'vengleap@slash.co', password: '828928132'},
        {id: 9, username: 'kevin@slash.co', password: 'nM!012323'},
        {id: 10, username: 'peter@slash.co', password: 'IHaveDYouWIllDie'},
        {id: 11, username: 'sean@slash.co', password: '51587E6D803A3D63645C445A56CC1EB3A0A55D3DC1F5C6751ECE2F89B917B23C'},
      ]);
    });
};
