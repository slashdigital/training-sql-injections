
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('articles').del()
    .then(function () {
      // Inserts seed entries
      return knex('articles').insert([
        {id: 1, title: 'Hello AI', description: 'User'},
        {id: 2, title: 'Quantum Realm', description: 'User'},
      ]);
    });
};
