
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('roles').del()
    .then(function () {
      // Inserts seed entries
      return knex('roles').insert([
        {id: 1, key: 'secret', value: '12312323'},
        {id: 2, key: 'phone', value: '909123'},
      ]);
    });
};
