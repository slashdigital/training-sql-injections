
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('roles').del()
    .then(function () {
      // Inserts seed entries
      return knex('roles').insert([
        {id: 1, key: 'user', value: 'User'},
        {id: 2, key: 'admin', value: 'Admin'},
      ]);
    });
};
