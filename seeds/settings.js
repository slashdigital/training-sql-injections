
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('settings').del()
    .then(function () {
      // Inserts seed entries
      return knex('settings').insert([
        {id: 1, key: 'Port', value: '123'},
        {id: 2, key: 'UserAlias', value: 'User_'},
      ]);
    });
};
