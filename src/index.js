const express = require('express');
const bodyParser = require('body-parser');
const nunjucks = require('nunjucks');
const path = require('path');
const app = express();
const urlencodedParser = bodyParser.urlencoded({ extended: false })

const loginCtrl = require('./login');

const port = process.env.PORT;

nunjucks.configure(path.join(__dirname, 'views'), {
    express: app,
    autoescape: true
});

app.set('view engine', 'html');

app.use(urlencodedParser);
app.get('/', (req, res) => res.render('index'));
app.post('/login', loginCtrl.login);

app.listen(port, () => console.log(`Training Injection app listening on port ${port}!`))